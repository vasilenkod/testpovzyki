//
//  CustomCell.swift
//  test
//
//  Created by Ihor Povzyk on 9/15/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit
import SWTableViewCell

class CustomCell: SWTableViewCell {

    @IBOutlet weak var blue: UIView!

    @IBOutlet weak var orange: UIView!
    @IBOutlet weak var fileModelImage: UIImageView!
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var modDate: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
