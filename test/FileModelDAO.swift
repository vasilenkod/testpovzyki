//
//  FileModelDAO.swift
//  test
//
//  Created by Ihor Povzyk on 9/15/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit

class FileModelDAO: NSObject {
    
    var data = Array<FileModel>()
    
    override init () {
        let first = FileModel(fileName: "Folder1", isFolder: "true", modDate: NSDate(), fileType: .FOLDER, isOrange: true, isBlue: true, folder: nil)
        let second = FileModel(fileName: "file.xxx", isFolder: "false", modDate: NSDate(), fileType:.FILE , isOrange: false, isBlue: true, folder: first)
        let third = FileModel(fileName: "files", isFolder: "true", modDate: NSDate(), fileType:.FOLDER , isOrange: true, isBlue: true, folder: first)
        let fourth = FileModel(fileName: "image.jpg", isFolder: "false", modDate: NSDate(), fileType:.IMAGE , isOrange: false, isBlue: true, folder: third)
        
        data.append(first)
        data.append(second)
        data.append(third)
        data.append(fourth)
    }
    
    
    public func getTestCase(parent: FileModel?) -> (Array<FileModel>) {
        
        
        
        
        var result = Array<FileModel>()
        
        if parent == nil {
            for i in 0..<data.count {
                if data[i].folder == nil {
                    result.append(data[i])
                }
            }
        }
        else {
            
            for i in 0..<data.count {
                if  data[i].folder != nil && (data[i].folder?.isEqual(parent))! {
                    result.append(data[i])
                }
            }
        }
        
        
        return result
    }
    
    
}
